package servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/create")
public class Create extends HttpServlet 
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 329388145150458270L;
	
	private void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException 
	{
		response.setContentType("text/html;charset=UTF-8");
		
		PrintWriter out = response.getWriter();
		
		double credit = Double.parseDouble(request.getParameter("credit"));
		int flat = Integer.parseInt(request.getParameter("flat"));
		double interest_rate = Double.parseDouble(request.getParameter("interest_rate"));
		String fixed_fee_string = request.getParameter("fixed_fee_string");
		
		String installment_type = request.getParameter("installment_type");
		
		double fixed_fee;
		if(fixed_fee_string == "")
			fixed_fee = 0;
		else
			fixed_fee = Double.parseDouble(fixed_fee_string);
		
		out.println("Podsumowanie");
		out.println("<br>Kwota wynosi: " + credit);
		out.println("<br>Ilo�� rat: " + flat);
		out.println("<br>interest_rate: " + interest_rate);
		out.println("<br>Op�ata sta�a: " + fixed_fee);
		
		if(installment_type.equals("m"))
			out.println("<br>Rodzaj rat: Malej�ca<br>");
		else
			out.println("<br>Rodzaj rat: Sta�a<br>");
						
		if(installment_type.equals("m")) 
		{
			double total_flat, interest_amount;
			double credit_amount = credit / flat;
			credit_amount *= 100;
			credit_amount = Math.round(credit_amount);
			credit_amount /= 100;
			
			out.println("<table border=\"2\" style=\"width:650px\">");
			out.println("<tr>");
			out.println("<td>");
			out.println("Nr raty");
			out.println("</td>");
			out.println("<td>");
			out.println("Kwota kapita�u");
			out.println("</td>");
			out.println("<td>");
			out.println("Kwota odsetek");
			out.println("</td>");
			out.println("<td>");
			out.println("Ca�kowita kwota raty");
			out.println("</td>");
			out.println("</tr>");
			
			for(int i = 0; i < flat; i++) {
				interest_amount = (credit - i * credit_amount) * (interest_rate / 100) / 12;
				interest_amount *= 100;
				interest_amount = Math.round(interest_amount);
				interest_amount /= 100;
				total_flat = credit_amount + interest_amount + fixed_fee;
				total_flat *= 100;
				total_flat = Math.round(total_flat);
				total_flat /= 100;
				out.println("<tr>");
					out.println("<td>");
						out.println(i + 1);
					out.println("</td>");
					out.println("<td>");
						out.println(credit_amount);
					out.println("</td>");
					out.println("<td>");
						out.println(interest_amount);
					out.println("</td>");
					out.println("<td>");
						out.println(total_flat);
					out.println("</td>");
				out.println("</tr>");
			}
			out.println("</table>");
		}
		else 
		{
			double q = 1 + (interest_rate / 100 / 12);
			q *= 1000000;
			long long_q = (long)q;
			q = Math.round(q);
			q = long_q / 1000000.0;
			System.out.println("q=" + q);
			double qn = Math.pow(q, flat);
			double installment = credit * qn * (q-1) / (qn - 1);
			out.println("Rata miesi�czna wynosi: " + installment);
		}
		out.close();
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException 
	{
		processRequest(request, response);
	}
}
